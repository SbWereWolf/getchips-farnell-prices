<?php

use TYPO3\CMS\Core\Database\DatabaseConnection;

/**
 * Created by PhpStorm.
 * User: anatoliybalabanov
 * Date: 04/02/2019
 * Time: 18:46
 */
class tx_getchips_farnellparser
{
    var $map = [
        'а' => 'a',
        'с' => 'c',
        'е' => 'e',
        'к' => 'k',
        'о' => 'o',
        'в' => 'b',
        'н' => 'h',
        'х' => 'x',
        'р' => 'p',
        'м' => 'm',
        'т' => 't',
    ];
    /**
     * @var array
     */
    var $files = array(
        'http://pfd.premierfarnell.com/farnell/resellers/eCat_Standard_USD_prices.zip',
        'http://pfd.premierfarnell.com/farnell/resellers/eCat_Standard_EE_Inventory.zip',
        'http://pfd.premierfarnell.com/farnell/resellers/eCat_Standard_EE_Data.zip'
    );

    public function parseFiles($files)
    {
        for ($i = 0; $i < count($files); $i++) {
            switch ($i) {
                case 0:
                    $this->getPrices($files[$i]);
                    break;
                case 1:
                    $this->getInvenroy($files[$i]);
                    break;
                case 2:
                    $this->getData($files[$i]);
                    break;
            }
        }
    }

    protected function getPrices($file)
    {

        /** @var TYPO3\CMS\Core\Database\DatabaseConnection $db */
        $db = $GLOBALS['TYPO3_DB'];
        $db->sql_query('
        CREATE TABLE IF NOT EXISTS tx_getchips_farnell_prices (
          `code` int(11) NOT NULL,
          `pricebreak` TEXT,
          PRIMARY KEY (`code`)
        )
        ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');
        $db->sql_query('TRUNCATE TABLE `tx_getchips_farnell_prices`');
        $fileLink = fopen($file, 'r');
        $n = 0;
        while ($row = fgetcsv($fileLink, 1024, '|')) {
            if ($n == 0) {
                $n++;
                continue;
            }
            $priceBreak = [];
            for ($i = 1; $i < 10; $i = $i + 2) {
                if ($row[$i] && $row[$i + 1]) {
                    $priceBreak[] = [
                        'quantity' => $row[$i],
                        'price' => $row[$i + 1]
                    ];
                }
            }
            $query = 'INSERT INTO `tx_getchips_farnell_prices` (code, pricebreak) VALUES (' . $row[0] . ',\'' . serialize($priceBreak) . '\')';
            $db->sql_query($query);
            $n++;
        }
    }

    protected function getInvenroy($file)
    {
        /** @var TYPO3\CMS\Core\Database\DatabaseConnection $db */
        $db = $GLOBALS['TYPO3_DB'];
        $db->sql_query('
        CREATE TABLE IF NOT EXISTS tx_getchips_farnell_inventory (
          `code` int(11) NOT NULL,
          `stock_gb` int(11) NOT NULL DEFAULT 0,
          `stock_lg` int(11) NOT NULL DEFAULT 0,
          `stock_p1` int(11) NOT NULL DEFAULT 0,
          `stock_pe` int(11) NOT NULL DEFAULT 0,
          PRIMARY KEY (`code`)
        )
        ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');
        $db->sql_query('TRUNCATE TABLE `tx_getchips_farnell_inventory`');
        $fileLink = fopen($file, 'r');
        $n = 0;
        while ($row = fgetcsv($fileLink, 1024, '|')) {
            if ($n == 0) {
                $n++;
                continue;
            }
            $query = 'INSERT INTO `tx_getchips_farnell_inventory` (code, stock_gb, stock_lg, stock_p1, stock_pe) 
            VALUES (' . $row[0] . ', ' . $row[1] . ', ' . $row[2] . ', ' . $row[3] . ',' . $row[4] . ' )';
            $db->sql_query($query);
            $n++;
        }

    }

    protected function getData($file)
    {
        /*
         * order code|
         * MPN|
         * Manufacturer name|
         * Short description|
         * Long description|
         * Class code|
         * UOM|
         * Pack size   MOQ|
         * RoHS Status|
         * Datasheet URL|
         * Image URL|
         * Source Brand
         */
        /** @var TYPO3\CMS\Core\Database\DatabaseConnection $db */
        $db = $GLOBALS['TYPO3_DB'];
        $db->sql_query('DROP TABLE tx_getchips_farnell_data');
        $db->sql_query('
        CREATE TABLE IF NOT EXISTS tx_getchips_farnell_data (
          `code` int(11) NOT NULL,
          `title` VARCHAR (255) NOT NULL DEFAULT "",
          `searchtitle` tinytext,
          `brand` VARCHAR (255) NOT NULL DEFAULT "",
          `crdate` int(11) NOT NULL,
          PRIMARY KEY (`code`)
        )
        ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');
        $db->sql_query('TRUNCATE TABLE `tx_getchips_farnell_data`');
        $fileLink = fopen($file, 'r');
        $n = 0;
        while ($row = fgetcsv($fileLink, 1024, '|')) {
            if ($n == 0) {
                $n++;
                continue;
            }
            $title = $db->fullQuoteStr($row[1] ?? '', 'tx_getchips_farnell_data');
            $searchtitle = $db->fullQuoteStr($this->replace($row[1] ?? ''), 'tx_getchips_farnell_data');
            $brand = $db->fullQuoteStr($row[2] ?? '', 'tx_getchips_farnell_data');

            $query = 'INSERT INTO `tx_getchips_farnell_data` (code, title, searchtitle, brand, crdate) 
                VALUES (' . ($row[0] ?? '') . ',' . $title . ',' . $searchtitle . ',' . $brand . ',' . time() . ')';
            $db->sql_query($query);
            $n++;
        }

    }

    public function replace($text, $that = '-')
    {

        $text = strip_tags(html_entity_decode($text));
        $text = strtr($text, $this->map);
        $text = preg_replace('/W/', $that, strip_tags($text));
        $text = $this->chicken_dick($text, $that);
        return $text;
    }

    public function chicken_dick($chicken, $dick = '/')
    {

        $chicken = preg_replace('/^([' . preg_quote($dick, '/') . ']+)/', '', $chicken);
        $chicken = preg_replace('/([' . preg_quote($dick, '/') . ']+)/', $dick, $chicken);
        $chicken = preg_replace('/([' . preg_quote($dick, '/') . ']+)$/', '', $chicken);

        return $chicken;
    }

}
