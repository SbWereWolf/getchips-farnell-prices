<?php

namespace TYPO3\CMS\Core\Database;

use mysqli;

class DatabaseConnection2
{
    /**
     * @var mysqli
     */
    private $link = null;
    private $isConnected = false;

    public function sql_query($query)
    {
        if (!$this->isConnected) {
            $this->connectDB();
        }
        $res = $this->query($query);

        return $res;
    }

    public function connectDB()
    {
        if ($this->isConnected) {
            return $this->link;
        }

        $this->link = mysqli_init();
        $connected = $this->link->real_connect(
            '127.0.0.1',
            'root',
            'admin',
            'arrow-order-api'
        );

        if ($connected) {
            $this->isConnected = TRUE;
        }

        return $this->link;
    }

    protected function query($query)
    {
        if (!$this->isConnected) {
            $this->connectDB();
        }
        return $this->link->query($query);
    }

    public function fullQuoteStr($str, $table, $allowNull = FALSE)
    {
        if (!$this->isConnected) {
            $this->connectDB();
        }
        if ($allowNull && $str === NULL) {
            return 'NULL';
        }

        return '\'' . $this->link->real_escape_string($str) . '\'';
    }

    public function sql_error()
    {
        return $this->link->error;
    }
}
