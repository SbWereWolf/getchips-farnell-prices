<?php

use TYPO3\CMS\Core\Database\DatabaseConnection2;

require_once 'DatabaseConnection2.php';
$GLOBALS['TYPO3_DB'] = new DatabaseConnection2();

$db = $GLOBALS['TYPO3_DB'];
$db->sql_query(
    'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED');
$db->sql_query('START TRANSACTION');
$db->sql_query('SET AUTOCOMMIT = 0');

$begin = time();

require_once 'class.tx_getchips_farnellparser.php';
$a = new tx_getchips_farnellparser();
$raw = [
    './files/eCat_Standard_USD_prices.csv',
    './files/eCat_Standard_EE_Inventory.txt',
    './files/eCat_Standard_EE_Data.txt',];
$a->parseFiles($raw);

echo "\n BEFORE COMMIT " . (time() - $begin);

$db->sql_query('SET AUTOCOMMIT = 1');
$db->sql_query('COMMIT');
echo "\n AFTER COMMIT " . (time() - $begin);
